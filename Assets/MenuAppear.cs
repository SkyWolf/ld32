﻿using UnityEngine;
using System.Collections;

public class MenuAppear : MonoBehaviour {
    public enum Status {
        Invisible,
        Appearing,
        Visible
    }
    public Status currentState = Status.Invisible;
    private float timeCollector = 0;

    private RectTransform rTransform;

    public float appearIn = 1f;

    void Awake() {
        rTransform = GetComponent<RectTransform>();
        rTransform.localScale = Vector3.zero;
    }

    void Update() {
        if(currentState == Status.Appearing) {
            timeCollector += Time.deltaTime;
            float percentage = Mathf.Min(1, timeCollector / appearIn);
            float apply = Mathf.SmoothStep(0, 1, percentage * 2);
            rTransform.localScale = new Vector3(apply, apply, 1);
            if(timeCollector >= appearIn)
                currentState = Status.Visible;
        }
    }

    public void Appear() {
        currentState = Status.Appearing;
    }
}
