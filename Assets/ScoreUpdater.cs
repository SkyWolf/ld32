﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof(Text))]
public class ScoreUpdater : MonoBehaviour {
    private Text toUpdate;

    void Awake() {
        toUpdate = GetComponent<Text>();
    }

    void Update() {
        toUpdate.text = "KILLS: " + ScoreManager.score + "\n" + "TARGET: " + ScoreManager.targetBeforeBoss;
    }
}
