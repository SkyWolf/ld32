﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour {
    public Transform enemiesContainer;
    public CharacterBehav character;

    public GameObject lose, win;
    public enum GState {
        Running,
        Lost,
        Won
    }
    public GState currentState = GState.Running;

    void Update() {
        if(currentState == GState.Running) {
            if(ScoreManager.score > ScoreManager.targetBeforeBoss &&
                enemiesContainer.childCount == 0) {
                win.SetActive(true);
                currentState = GState.Won;
            }
            if(character.currentHealth <= 0) {
                lose.SetActive(true);
                currentState = GState.Lost;
            }
        }
        else {
            if(Input.GetMouseButtonDown(0))
                Application.LoadLevel("mainmenu");
        }
    }
}
