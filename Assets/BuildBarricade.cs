﻿using UnityEngine;
using System.Collections;

public class BuildBarricade : MonoBehaviour {
    private Transform myTransform;

    public GameObject barricadePart;
    private SpriteRenderer bPSpriteRenderer;

    public void Build(float howWide) {
        if(!Application.isPlaying)
            return;


        if(myTransform == null)
            myTransform = transform;
        if(bPSpriteRenderer == null)
            bPSpriteRenderer = barricadePart.GetComponent<SpriteRenderer>();

        int childCount = myTransform.childCount;
        for(int i = 0; i < childCount; i++) {
            Destroy(myTransform.GetChild(i).gameObject);
        }
        Sprite bPSprite = bPSpriteRenderer.sprite;
        float barricadeSize = bPSprite.rect.width / bPSprite.pixelsPerUnit;

        int howMany = (int)((howWide / barricadeSize) + 1);
        if(howMany % 2 == 0)
            howMany++;

        int halfHW = howMany / 2;
        Quaternion zeroRot = Quaternion.Euler(0, 0, 0);
        for(int i = -halfHW; i <= halfHW; i++) {
            GameObject newB = (GameObject)Instantiate(barricadePart);
            Transform newBT = newB.transform;

            newBT.parent = myTransform;
            newBT.localPosition = new Vector2(i * barricadeSize, 0);
            newBT.localRotation = zeroRot;
        }
    }
}
