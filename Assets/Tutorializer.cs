﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tutorializer : MonoBehaviour {
    public Text text01;
    public Text text02;
    public Text text03;
    public Text text04;
    
    public AnimatedArrow arrow;

    public Text text05;
    public Text text06;
    public Text text07;

    public Image cursor;

    public Text text08;
    public Text text09;
    public Text text10;

    public CharacterBehav character;
    public FruitSpawner fSpawner;
    public Orbiting cOrbiting;

    public float textSpeed;

    void Start() {
        StartCoroutine(Tutorial());
    }

    void Update() {
        if(Input.GetMouseButtonDown(0))
            Application.LoadLevel("gameplayTrue");
    }

    private Transform orbit;
    private IEnumerator Tutorial() {
        this.enabled = false;
        character.registerHowMuchMovement = true;
        character.disableMovement = true;
        text01.enabled = true;
        yield return new WaitForSeconds(textSpeed);
        text01.enabled = false;
        text02.enabled = true;
        yield return new WaitForSeconds(textSpeed);
        text02.enabled = false;
        character.disableMovement = false;
        text03.enabled = true;
        while(character.howMuchIMoved < 2.5f)
            yield return new WaitForSeconds(0.2f);
        character.registerHowMuchMovement = false;
        text03.enabled = false;
        text04.enabled = true;
        orbit = character.transform.FindChild("Orbit");
        fSpawner.RequestDistance(character.transform.position, 1f);
        Transform spawnedFruit = fSpawner.SpawnOne().transform;
        arrow.GetComponent<RectTransform>().position = spawnedFruit.position;
        arrow.enabled = true;

        cOrbiting.cantShoot = true;
        while(orbit.childCount == 0)
            yield return new WaitForSeconds(0.2f);
        text04.enabled = false;
        arrow.enabled = false;
        text05.enabled = true;
        yield return new WaitForSeconds(textSpeed);
        text05.enabled = false;
        text06.enabled = true;
        yield return new WaitForSeconds(textSpeed);
        text06.enabled = false;
        text07.enabled = true;
        cOrbiting.cantShoot = false;    
        //cOrbiting
        
        Transform orbitingT = cOrbiting.transform;
        Transform childT = cOrbiting.transform.GetChild(0);
        do {
            Vector3 direction = childT.position - orbitingT.position;
            cursor.rectTransform.position = childT.position + (direction.normalized * 0.3f);
            cursor.enabled = true;
            yield return new WaitForEndOfFrame();
        } while(orbit.childCount != 0);
        cursor.enabled = false;
        text07.enabled = false;
        text08.enabled = true;
        yield return new WaitForSeconds(textSpeed);
        text08.enabled = false;
        text09.enabled = true;
        yield return new WaitForSeconds(textSpeed);
        text09.enabled = false;
        text10.enabled = true;
        this.enabled = true;
    }
}