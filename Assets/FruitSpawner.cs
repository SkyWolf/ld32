﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(BoundsDescriptor))]
public class FruitSpawner : MonoBehaviour {
    private BoundsDescriptor myBounds;
    private AudioSource mySource;
    private FruitPoolMan fruitPool;

    public float minSpawnTimer;
    public float maxSpawnTimer;
    private float spawnIn;
    public float borderTolerance;
    private float timeCollector;

    public bool spawnAutomatically = true;

    public AudioClip audioSpawningFruit;

    private enum SpawnableList {
        Banana,
        Melon,
        Strawberry,
        Grapes,
        Coconut,
        Tomato,
        SPAWNABLE_COUNT
    }

    private float[] weights = new float[] {
        1.50f,
        1.00f,
        1.00f,
        1.25f,
        1.50f,
        1.25f
    };
    private float totalWeight;

    //This request system is horrible and hacky. >_>
    private bool typeRequest;
    private FruitPoolMan.FruitType whatType;

    private bool distanceRequest;
    private Vector3 fromWhere;
    private float whatDistance;

    void Awake() {
        myBounds = GetComponent<BoundsDescriptor>();
        mySource = GetComponent<AudioSource>();
        fruitPool = GameObject.FindGameObjectWithTag("Pools").GetComponent<ObjectPoolList>().fruitPools;

        totalWeight = 0;
        for(int i = 0; i < weights.Length; i++)
            totalWeight += weights[i];

        timeCollector = 0;
        spawnIn = (Random.value * (maxSpawnTimer - minSpawnTimer)) + minSpawnTimer;
    }

    void Update() {
        if(spawnAutomatically) {
            timeCollector += Time.deltaTime;

            if(timeCollector >= spawnIn) {
                SpawnOne();

                timeCollector = 0;
                spawnIn = (Random.value * (maxSpawnTimer - minSpawnTimer)) + minSpawnTimer;
            }
        }
    }

    public void RequestType(FruitPoolMan.FruitType _whatType) {
        whatType = _whatType;
    }

    public void RequestDistance(Vector3 _fromWhere, float _whatDistance) {
        fromWhere = _fromWhere;
        whatDistance = _whatDistance;
    }

    public GameObject SpawnOne() {
        mySource.PlayOneShot(audioSpawningFruit);
        FruitPoolMan.FruitType ftSpawn;

        if(typeRequest) {
            ftSpawn = whatType;
            typeRequest = false;
        }
        else {

            int ixToSpawn = weights.Length - 1;
            bool found = false;

            float toSpawn = Random.value * totalWeight;
            float total = 0;
            for(int i = 0; i < weights.Length; i++) {
                total += weights[i];
                if(!found && toSpawn < total) {
                    ixToSpawn = i;
                    found = true;
                }
            }

            ftSpawn = (FruitPoolMan.FruitType)ixToSpawn;
        }

        Vector3 fPosition;
        if(distanceRequest) {
            fPosition = fromWhere;
            while(Vector3.Distance(fPosition, fromWhere) < whatDistance)
                fPosition = myBounds.RandomPointInsideBounds(borderTolerance);
        }
        else
            fPosition = myBounds.RandomPointInsideBounds(borderTolerance);

        GameObject fruit = fruitPool.GetFruit(ftSpawn);
        fruit.GetComponent<FruitFire>().Spawn(fPosition);

        return fruit;
    }
}
