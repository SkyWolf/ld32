﻿using UnityEngine;
using System.Collections;

public class FetchCharacterLife : MonoBehaviour {
    private CharacterBehav theCharacter;
    public HeartVisualize visualization;

    void Awake() {
        theCharacter = GameObject.FindGameObjectWithTag("MainChar").GetComponent<CharacterBehav>();
    }

    void Update() {
        visualization.Visualize(theCharacter.currentHealth, theCharacter.MAX_HEALTH);
    }
}
