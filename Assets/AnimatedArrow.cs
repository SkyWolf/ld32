﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof(Image))]
public class AnimatedArrow : MonoBehaviour {
    private float originalHeight;
    private RectTransform myTransform;
    private Image myImage;

    void Awake() {
        myTransform = GetComponent<RectTransform>();
        myImage = GetComponent<Image>();
    }

    private float timeCollector;
    //Should always be moved before being enabled
    void OnEnable() {
        timeCollector = 0;
        originalHeight = myTransform.position.y;
        myImage.enabled = true;
    }
	
	void Update () {
        timeCollector += Time.deltaTime * 0.2f;
        Vector3 tmpPosition = myTransform.position;
        tmpPosition.y = originalHeight + Mathf.PingPong(timeCollector, 0.2f);
        myTransform.position = tmpPosition;
	}

    void OnDisable() {
        myImage.enabled = false;
    }
}
