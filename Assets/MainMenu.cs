﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour {
    public MenuAppear fruit;
    public MenuAppear salad;
    public MenuAppear inspace;
    public MenuAppear edition;
    public MenuAppear wlff;

    void Start() {
        StartCoroutine(Menu());
    }

    private IEnumerator Menu() {
        fruit.Appear();
        while(fruit.currentState != MenuAppear.Status.Visible)
            yield return new WaitForEndOfFrame();

        salad.Appear();
        while(salad.currentState != MenuAppear.Status.Visible)
            yield return new WaitForEndOfFrame();

        inspace.Appear();
        while(inspace.currentState != MenuAppear.Status.Visible)
            yield return new WaitForEndOfFrame();

        edition.Appear();
        while(edition.currentState != MenuAppear.Status.Visible)
            yield return new WaitForEndOfFrame();

        wlff.Appear();
        while(wlff.currentState != MenuAppear.Status.Visible)
            yield return new WaitForEndOfFrame();
    }

    public void StartTutorial() {
        Application.LoadLevel("tutorial");
    }

    public void StartGame() {
        Application.LoadLevel("gameplayTrue");
    }
}
