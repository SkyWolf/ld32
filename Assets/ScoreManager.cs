﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {
    public static int score { get; private set; }
    public const int targetBeforeBoss = 20;

    public static void AddPoint() {
        score++;
    }

    void Awake() {
        score = 0;
    }
}
