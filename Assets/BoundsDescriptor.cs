﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BoundsDescriptor : MonoBehaviour {
    private Camera myCamera;
    private HorizontalSize horizontalSize;

    public BoxCollider2D north, east, south, west;
    public float triggerSize;
    public float playFieldHeight;

    private float oldHorizontalSize, oldPlayFieldHeight, oldTriggerSize;
    public BuildBarricade bNorth, bSouth;

    void Awake() {
        GameObject cameraGO = GameObject.FindGameObjectWithTag("MainCamera");
        horizontalSize = cameraGO.GetComponent<HorizontalSize>();

        FixBounds();
    }

    public bool AmIInBounds(Vector3 position) {
        if( position.x < -oldHorizontalSize ||
            position.x > oldHorizontalSize ||
            position.y < -(oldPlayFieldHeight / 2) ||
            position.y > (oldPlayFieldHeight / 2))
            return false;
        return true;
    }

    private void FixBounds() {
        oldHorizontalSize = horizontalSize.horizontalOrthographicSize;
        oldPlayFieldHeight = playFieldHeight;
        oldTriggerSize = triggerSize;

        float halfSize = triggerSize / 2;
        float doubleTSize = triggerSize * 2;
        float halfHeight = playFieldHeight / 2;
        float doubleHSize = horizontalSize.horizontalOrthographicSize * 2;

        Vector2 NSTriggerSize = new Vector3(doubleHSize, triggerSize);
        Vector2 EWTriggerSize = new Vector2(triggerSize, playFieldHeight + doubleTSize);

        north.offset = new Vector3( 0,
                                    halfHeight + halfSize);
        north.size = NSTriggerSize;

        bNorth.transform.localPosition = new Vector3(0, halfHeight, 0);
        bNorth.Build(NSTriggerSize.x);

        south.offset = new Vector3( 0,
                                    -(halfHeight + halfSize));
        south.size = NSTriggerSize;

        bSouth.transform.localPosition = new Vector3(0, -halfHeight, 0);
        bSouth.Build(NSTriggerSize.x);

        east.offset = new Vector2(  horizontalSize.horizontalOrthographicSize + halfSize,
                                    0);
        east.size = EWTriggerSize;

        west.offset = new Vector2(  -(horizontalSize.horizontalOrthographicSize + halfSize),
                                    0);
        west.size = EWTriggerSize;
    }

    public Vector2 RandomPointInsideBounds(float tolerance) {
        float dTolerance = tolerance * 2;

        float x =   (Random.value * ((oldHorizontalSize * 2) - dTolerance))
                    - (oldHorizontalSize - tolerance);
        float y =   (Random.value * ((oldPlayFieldHeight) - dTolerance))
                    - ((oldPlayFieldHeight / 2) - tolerance);
        return new Vector2(x, y);
    }
	
	void Update () {
        if( oldHorizontalSize != horizontalSize.horizontalOrthographicSize ||
            oldPlayFieldHeight != playFieldHeight ||
            oldTriggerSize != triggerSize)

            FixBounds();
	}
}
