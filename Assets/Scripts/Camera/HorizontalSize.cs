﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent (typeof(Camera))]
public class HorizontalSize : MonoBehaviour {
    public float horizontalOrthographicSize = 3;
    private float oldHOS = -1;

    private int myScreenWidth, myScreenHeight;
    private Camera myCamera;

    void Awake() {
        myCamera = GetComponent<Camera>();
    }

    private void FixSize() {
        myScreenWidth = Screen.width;
        myScreenHeight = Screen.height;
        oldHOS = horizontalOrthographicSize;

        myCamera.orthographicSize = myScreenHeight * (horizontalOrthographicSize / myScreenWidth);
    }

    void Update() {
        if(Screen.width != myScreenWidth ||
            Screen.height != myScreenHeight ||
            horizontalOrthographicSize != oldHOS)

            FixSize();
    }
}
