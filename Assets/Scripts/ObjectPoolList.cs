﻿using UnityEngine;
using System.Collections;

public class ObjectPoolList : MonoBehaviour {
    public ObjectPool rowPool;
    public ObjectPool piecePool;
    public FruitPoolMan fruitPools;
    public ObjectPool particlePool;
    public ObjectPool lifeFlasherPool;
}