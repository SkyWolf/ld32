﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BackgroundCreator))]
public class BackgroundMover : MonoBehaviour {
    private Transform myTransform;
    private Camera myCamera;

    private BackgroundCreator backgroundCreator;
    private Transform backgroundContainer;

    public float backgroundScrollingSpeed = 1f;

    void Awake() {
        myTransform = transform;
        myCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        backgroundCreator = myTransform.GetComponent<BackgroundCreator>();
        backgroundContainer = backgroundCreator.backgroundContainer;
    }

    void Update() {
        for(int i = 0; i < backgroundContainer.childCount; i++) {
            Transform iChild = backgroundContainer.GetChild(i);

            iChild.position += Vector3.up * backgroundScrollingSpeed * Time.deltaTime;
        }

        Transform firstChild = backgroundContainer.GetChild(0);
        if(firstChild.position.y - (backgroundCreator.size.y / 2) > myCamera.orthographicSize)
            backgroundCreator.ReturnRow(firstChild);

        Transform lastChild = backgroundContainer.GetChild(backgroundContainer.childCount - 1);
        if(lastChild.position.y - (backgroundCreator.size.y / 2) > -myCamera.orthographicSize)
            backgroundCreator.AddARow(lastChild.position.y - backgroundCreator.size.y);
    }
}