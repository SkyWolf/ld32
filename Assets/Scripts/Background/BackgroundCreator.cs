﻿using UnityEngine;
using System.Collections;

public class BackgroundCreator : MonoBehaviour {
    public Transform backgroundContainer { get; private set; }
    public ObjectPoolList poolList { get; private set; }

    private Transform myTransform;
    private Camera myCamera;

    public Sprite[] backgrounds;
    public int sortingLayer = 0;

    [HideInInspector]
    public Vector2 size = Vector2.zero;
    private Vector2 screenWorldSize = Vector2.zero;

    private int oldScreenWidth, oldScreenHeight;
    private int verticalScreens, horizontalScreens;

    void Awake() {
        myTransform = transform;
        myCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        GameObject backgroundContainerGO = new GameObject();
        backgroundContainer = backgroundContainerGO.transform;
        backgroundContainer.name = "Container";
        backgroundContainer.parent = myTransform;

        poolList = GameObject.FindGameObjectWithTag("Pools").GetComponent<ObjectPoolList>();

        Initialize();
    }

    private void Initialize() {
        int childs = backgroundContainer.childCount;
        for(int i = 0; i < childs; i++)
            Destroy(backgroundContainer.GetChild(i).gameObject);

        RecalculateSizes();

        int hHS = verticalScreens / 2;
        for(int i = hHS; i >= -hHS; i--) {
            AddARow(transform.position.y + (i * size.y));
        }
    }

    private void RecalculateSizes() {
        oldScreenWidth = Screen.width;
        oldScreenHeight = Screen.height;

        screenWorldSize.y = myCamera.orthographicSize * 2;
        screenWorldSize.x = Screen.width * (screenWorldSize.y / Screen.height);

        Sprite sample = backgrounds[0];
        size.x = sample.rect.width / sample.pixelsPerUnit;
        size.y = sample.rect.height / sample.pixelsPerUnit;

        horizontalScreens = (int)(screenWorldSize.x / size.x) + 1;
        if(horizontalScreens % 2 == 0)
            horizontalScreens++;
        verticalScreens = (int)(screenWorldSize.y / size.y) + 1;
        if(verticalScreens % 2 == 0)
            verticalScreens++;
    }

    public Transform AddARow(float absoluteHeight) {
        int hVS = horizontalScreens / 2;
        
        Transform tempRow = poolList.rowPool.GetOne().transform;
        tempRow.parent = backgroundContainer;
        tempRow.position = new Vector2(0, absoluteHeight);

        for(int j = -hVS; j <= hVS; j++) {
            Transform tempGOT = poolList.piecePool.GetOne().transform;
            tempGOT.parent = tempRow;
            tempGOT.localPosition = new Vector2(j * size.x, 0);
            tempGOT.name = j.ToString();

            SpriteRenderer tempRen = tempGOT.GetComponent<SpriteRenderer>();
            tempRen.sprite = backgrounds[Random.Range(0, backgrounds.Length)];
            tempRen.sortingOrder = sortingLayer;
        }

        return tempRow;
    }

    private void FixHeight() {
        RecalculateSizes();

        int hHS = Mathf.Abs(backgroundContainer.childCount - verticalScreens);
        if(verticalScreens < backgroundContainer.childCount) {
            for(int i = 0; i < hHS; i++) {
                Transform child = backgroundContainer.GetChild(backgroundContainer.childCount - 1);
                ReturnRow(child);
            }
            for(int i = 0; i < hHS; i++) {
                Transform child = backgroundContainer.GetChild(0);
                ReturnRow(child);
            }
        }
        else {
            for(int i = 0; i < hHS; i++) {
                float newHeight = backgroundContainer.GetChild(backgroundContainer.childCount - 1).position.y - size.y;
                AddARow(newHeight);
            }
            for(int i = 0; i < hHS; i++) {
                float newHeight = backgroundContainer.GetChild(backgroundContainer.childCount - 1).position.y + size.y;
                Transform newRow = AddARow(newHeight);
                newRow.SetAsFirstSibling();
            }
        }
    }

    public void ReturnRow(Transform child) {
        int count = child.childCount;
        for(int x = 0; x < count; x++)
            poolList.piecePool.ReturnOne(child.GetChild(0).gameObject);
        poolList.rowPool.ReturnOne(child.gameObject);
    }

    private void FixWidth() {
        RecalculateSizes();

        for(int i = 0; i < backgroundContainer.childCount; i++) {
            Transform tempRow = backgroundContainer.GetChild(i);
            int oldColumns = backgroundContainer.GetChild(i).childCount;
            if(horizontalScreens > oldColumns) {
                int toAdd = (horizontalScreens - oldColumns) / 2;
                for(int j = 0; j < toAdd; j++) {
                    Transform tempGOT = poolList.piecePool.GetOne().transform;
                    tempGOT.parent = tempRow;
                    Debug.Log(oldColumns);
                    int index = -((oldColumns / 2) + (j + 1));
                    tempGOT.localPosition = new Vector2(index * size.x, 0);
                    tempGOT.name = j.ToString();

                    SpriteRenderer tempRen = tempGOT.GetComponent<SpriteRenderer>();
                    tempRen.sprite = backgrounds[Random.Range(0, backgrounds.Length)];
                    tempRen.sortingOrder = sortingLayer;
                }
                for(int j = 0; j < toAdd; j++) {
                    Transform tempGOT = poolList.piecePool.GetOne().transform;
                    tempGOT.parent = tempRow;
                    int index = (oldColumns / 2) + (j + 1);
                    tempGOT.localPosition = new Vector2(index * size.x, 0);
                    tempGOT.name = j.ToString();

                    SpriteRenderer tempRen = tempGOT.GetComponent<SpriteRenderer>();
                    tempRen.sprite = backgrounds[Random.Range(0, backgrounds.Length)];
                    tempRen.sortingOrder = sortingLayer;
                }
            }
        }
    }
}
