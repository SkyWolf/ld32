﻿using UnityEngine;
using System.Collections;

public class ParticleLife : MonoBehaviour {
    private Vector3 direction;
    private float speed;

    private float dieIn;
    private ObjectPool myPool;

    private Transform myTransform;
    private SpriteRenderer myRenderer;
    private Color buildUpColor;

    private float timeCollector, percentage;

    public void Live(Sprite representation, Color spriteColor, Vector3 startPosition, Vector3 direction, float speed, float dieIn, ObjectPool myPool) {
        this.enabled = true;

        if(myTransform == null)
            myTransform = transform;
        if(myRenderer == null)
            myRenderer = GetComponent<SpriteRenderer>();

        myTransform.position = startPosition;
        this.direction = direction;
        this.speed = speed;

        buildUpColor = spriteColor;
        buildUpColor.a = 1;
        myRenderer.color = buildUpColor;

        myRenderer.sprite = representation;

        this.dieIn = dieIn;
        this.myPool = myPool;

        timeCollector = 0;
    }

    
    void Update() {
        timeCollector += Time.deltaTime;

        myTransform.Translate(direction * speed * Time.deltaTime);

        percentage = timeCollector / dieIn;
        buildUpColor.a = Mathf.Lerp(1, 0, timeCollector / dieIn);
        myRenderer.color = buildUpColor;

        if(percentage >= 1) {
            this.enabled = false;
            myPool.ReturnOne(gameObject);
        }
    }
}