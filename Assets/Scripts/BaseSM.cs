﻿using UnityEngine;
using System.Collections;

public abstract class BaseSM : MonoBehaviour {
    internal delegate void State();

    internal State[] exitArray;
    internal State[] enterArray;
    internal State[] updateArray;

    //[HideInInspector]
    public int currentState, lastState, lastActiveState;

    /// <summary> 
    /// Used to initialize the state machine and feed the enumerator.
    /// </summary>
    /// <param name="actualEnumeratorType">This must be typeof(WhateverEnumerator). Note: 0th state is the state the machine will start on.</param>
    protected void Initialize(int numStates, int startState) {
        exitArray = new State[numStates];
        enterArray = new State[numStates];
        updateArray = new State[numStates];

        // This is zero because we assume the 0th state is the one which the SM starts in.
        currentState = startState;
        lastState = lastActiveState = startState;

        Config();

        this.enabled = true;
    }

    /// <summary> 
    /// This must be overridden and it's where arrays must be fillen accordigly.
    /// </summary>
    internal abstract void Config();

    /// /// <param name="newState">New state index.</param>
    protected void ChangeState(int newState) {
        if((newState) < 0 || (newState) >= updateArray.Length)
            return;
        lastActiveState = currentState;
        currentState = newState;
    }

    void Update() {
        if(currentState != lastState) {
            if((lastState) < exitArray.Length && exitArray[lastState] != null)
                exitArray[lastState]();
            if(enterArray[currentState] != null)
                enterArray[currentState]();

            lastState = currentState;
        }

        if(updateArray[currentState] != null)
            updateArray[currentState]();
    }

}
