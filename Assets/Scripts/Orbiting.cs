﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Orbiting : MonoBehaviour {
    private Transform myTransform;

    //orbiting rules
    public float orbitingSpeed;
    public float orbitRadius;
    private Vector3 spawnPosition;

    private FruitPoolMan fruitsManager;

    private int lastIndexAdded;

    public bool cantShoot = false;

    public float MAX_ANGLE_TOLERANCE = 30f;

	void Awake () {
        myTransform = this.transform;

        spawnPosition = new Vector3(0, orbitRadius, 0);

        fruitsManager = GameObject.FindGameObjectWithTag("Pools").GetComponent<ObjectPoolList>().fruitPools;
	}
	
	void Update () {
        myTransform.localRotation = Quaternion.AngleAxis(orbitingSpeed * Time.deltaTime, -myTransform.forward) *
                myTransform.localRotation;

        if(!cantShoot) {
            if(Input.GetMouseButtonDown(0))
                ShootFruit();

            //TODO: Remove
            if(Input.GetMouseButtonDown(1))
                CreateFruit();
        }
	}

    public Vector3 GetInOrbitProjectedPosition(Vector3 position) {
        Vector3 direction = position - myTransform.position;
        return myTransform.position + (direction.normalized * orbitRadius);
    }

    public Vector3 GetOrbitCenter() {
        return myTransform.position;
    }

    public void CreateFruit() {
        int fruitIdx = Random.Range(0, (int)FruitPoolMan.FruitType.size - 2);
        GameObject newFruit = fruitsManager.GetFruit((FruitPoolMan.FruitType)fruitIdx);
        newFruit.GetComponent<FruitFire>().Spawn(spawnPosition);

        //AddExistingFruit(newFruit.transform);
    }

    public void AddExistingFruit(Transform fruitT) {
        //int entranceIdx = GetChildIdxByPosition(fruitT.position, myTransform.childCount + 1);
        FruitFire fruitF = fruitT.GetComponent<FruitFire>();

        SortFruits(fruitF.GetPosition());

        fruitT.parent = myTransform;
        fruitT.SetSiblingIndex(0);
        fruitF.EnterInOrbit();

        lastIndexAdded = 0;

        FixChildrenInOrbit(fruitT);
    }

    private void SortFruits(Vector3 firstFruit) {
        float minAngle = 400f;
        int minIdx = 0;
        Vector3 toFirstFruitDir = firstFruit - myTransform.position;
        Vector3 toFirstFruitDirOrt = Quaternion.AngleAxis(90, -Vector3.forward) * toFirstFruitDir;
        for(int i = 0; i < myTransform.childCount; i++) {
            Vector3 toDir = myTransform.GetChild(i).position - myTransform.position;
            float angle = Vector3.Angle(toFirstFruitDir, toDir);
            if(Vector3.Angle(toFirstFruitDirOrt, toDir) > 90)
                angle = 360 - angle;

            if(angle < minAngle) {
                minAngle = angle;
                minIdx = i;
            }
        }

        int childCount = myTransform.childCount;
        List<Transform> tmpList = new List<Transform>();
        for(int i = 0; i < childCount; i++) {
            tmpList.Add(myTransform.GetChild((minIdx + i) % childCount));
        }
        for(int i = 0; i < childCount; i++) {
            myTransform.GetChild(0).parent = null;
        }
        for(int i = 0; i < childCount; i++) {
            tmpList[0].parent = myTransform;
            tmpList.RemoveAt(0);
        }
    }

    public void ShootFruit() {
        if(myTransform.childCount == 0)
            return;

        //angle and dir of selection
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPos.z = 0;

        int idx = GetChildIdxByPosition(mouseWorldPos, myTransform.childCount);
        FruitFire fruit = myTransform.GetChild(idx).GetComponent<FruitFire>();

        //check max tolerance
        float angle = Vector3.Angle(fruit.GetPosition() - myTransform.position, mouseWorldPos - myTransform.position);
        if(angle > MAX_ANGLE_TOLERANCE)
            return;

        //shoot it!
        fruit.PlayShoot(myTransform.position);

        lastIndexAdded = 0;

        //finally fix the remaining fruits children
        FixChildrenInOrbit();
    }

    private int GetChildIdxByPosition(Vector3 pos, int maxChildren) {
        if(maxChildren == 0)
            return 0;

        Vector3 selectionDir = pos - myTransform.position;

        Vector3 toFirstElem = myTransform.GetChild(0).position - myTransform.position;
        Vector3 toFirstElemOrt = Quaternion.AngleAxis(90, -Vector3.forward) * toFirstElem;

        float deltaAngle = Vector3.Angle(toFirstElem, selectionDir);
        if(Vector3.Angle(toFirstElemOrt, selectionDir) >= 90f)
            deltaAngle = 360 - deltaAngle;


        //Debug.Log("delta angle = " + deltaAngle);

        //get the fruit in direction
        float baseDegree = 360f / maxChildren;
        float fixedDegree = deltaAngle + baseDegree / 2f;
        int idx = Mathf.FloorToInt(fixedDegree / baseDegree) % maxChildren;
        //Debug.Log("idx = " + idx);
        return idx;
    }

    private void FixChildrenInOrbit(Transform lastAdded = null) {
        if(myTransform.childCount == 0)
            return;
        
        //save the starting position to calculate in orbit positions
        Vector3 startingPosition = lastAdded != null ? lastAdded.position :
            myTransform.GetChild(0).position;
        Vector3 mainDirection = startingPosition - myTransform.position;

        float subAngle = 360f / myTransform.childCount;
        for(int i = 0; i < myTransform.childCount; i++) {
            int idx = (lastIndexAdded + i) % myTransform.childCount;
           /* myTransform.GetChild(i).localPosition = Quaternion.AngleAxis(subAngle * i, -myTransform.forward) *
                spawnPosition;*/
            FruitFire fruit = myTransform.GetChild(idx).GetComponent<FruitFire>();

            //calculate angle bw expected position and current position;

            Vector3 correctDirection = Quaternion.AngleAxis(subAngle * i, -myTransform.forward) * mainDirection;
            Vector3 orthogDir = Quaternion.AngleAxis(90, -myTransform.forward) * correctDirection;

            //float expectedAngle = subAngle * i;
            Vector3 toFruitDir = fruit.GetPosition() - myTransform.position;
            float deltaAngle = Vector3.Angle(toFruitDir, correctDirection);
            if(Vector3.Angle(toFruitDir, orthogDir) < 90)
                deltaAngle = - deltaAngle;

            fruit.EnterInOrbit(deltaAngle);
        }
    }
}
