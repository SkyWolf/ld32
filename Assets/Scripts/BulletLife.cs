﻿using UnityEngine;
using System.Collections;

public class BulletLife : MonoBehaviour {

    private Transform myTransform;
    private GameObject myGameObject;
    private Vector3 currentDirection;

    public float SPEED = 2.0f;

    public bool fixAngleBeforeFire;

	// Use this for initialization
	void Awake () {
        myTransform = this.transform;
	}

    public void Fire(Vector3 position, Vector3 direction) {
        this.gameObject.SetActive(true);
        currentDirection = direction.normalized;
        myTransform.position = position;

        if(fixAngleBeforeFire) {
            direction.Normalize();

            float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            myTransform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
    }
	
	// Update is called once per frame
	void Update () {
        myTransform.position += currentDirection * SPEED * Time.deltaTime;
	}

    void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "MainChar") {
            CharacterBehav character = other.GetComponent<CharacterBehav>();
            character.HitByEnemy(1);
            this.gameObject.SetActive(false);
            //GameObject.Destroy(this.gameObject);
        }
    }
}
