﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FruitPoolMan : MonoBehaviour {
    public int sizePerFruit;
    public GameObject[] fruitPrefabs = new GameObject[(int)FruitType.size];
    private ObjectPool[] fruitPools;

    private Transform myTransform;

    public enum FruitType {
        banana,
        melon,
        straw,
        grape,
        coconut,
        tomato,
        melonPiece,
        grapePiece,
        size
    }

    void Awake() {
        myTransform = this.transform;

        fruitPools = new ObjectPool[(int)FruitType.size];
        for(int i=fruitPools.Length - 1; i >= 0; i--){
            GameObject pool = new GameObject();
            fruitPools[i] = pool.AddComponent<ObjectPool>();
            fruitPools[i].toPool = fruitPrefabs[i];
            fruitPools[i].startWith = sizePerFruit;
            pool.transform.parent = myTransform;
            fruitPools[i].Initialize();
        }
    }

    public GameObject GetFruit(FruitType type) {
        return fruitPools[(int)type].GetOne();
    }

    public void ReturnFruit(GameObject fruit, FruitType type) {
        fruitPools[(int)type].ReturnOne(fruit);
    }
}
