﻿using UnityEngine;
using System.Collections;

public class Strawberry : FruitFire {

    public float MAX_BOUNCE_ANGLE;

    internal override bool HitEnemy(EnemyBehav enemy, bool deleteWhenHit) {
        //start bouncing effect
        bool success = base.HitEnemy(enemy, false);
        StartBouncingEffect();
        return success;
    }

    private void StartBouncingEffect() {
        //calculate random angle
        float randomAngle = Random.value * MAX_BOUNCE_ANGLE * 2;
        randomAngle -= MAX_BOUNCE_ANGLE;

        //calculate bouncing dir
        Vector3 bouncingDir = Quaternion.AngleAxis(randomAngle, -Vector3.forward) * (-movementDir);
        movementDir = bouncingDir.normalized;
    }
}
