﻿using UnityEngine;
using System.Collections;

public class Banana : FruitFire {
    //arc movement params
    private Vector3 startArcDirection;
    private Vector3 endArcDirection;
    public float START_ARC_ANGLE;
    private float angleSpeed;
    public float MIN_STRAIGHT;

    private Vector3 pivot;

    public bool straight;


    internal override void EnterShot() {
        base.EnterShot();

        movementSpeed = SHOOT_SPEED;
        straight = true;

        //calculate initial arc angles
        startArcDirection = Quaternion.AngleAxis(START_ARC_ANGLE, -Vector3.forward) * shootDir;
        endArcDirection = Quaternion.AngleAxis(-START_ARC_ANGLE, -Vector3.forward) * shootDir;
        endArcDirection = -endArcDirection;

        //calculate arc distance
        float distanceByPivot = MIN_STRAIGHT / Mathf.Cos(START_ARC_ANGLE * Mathf.Deg2Rad);
        //Debug.Log("pivot distance = " + distanceByPivot);
        pivot = myTransform.position + (shootDir.normalized * distanceByPivot);

        float radius = MIN_STRAIGHT * Mathf.Tan(START_ARC_ANGLE * Mathf.Deg2Rad);
        float internalAngle = 180 - START_ARC_ANGLE - 90;
        float arcAngle = 360 - internalAngle * 2;
        float arcDistance = (2 * radius * Mathf.PI * arcAngle) / 360;
        float seconds = arcDistance / SHOOT_SPEED;
        angleSpeed = arcAngle / seconds;

        movementDir = startArcDirection.normalized;
    }
    internal override void UpdateShot() {
        base.UpdateShot();

        if(straight) {
            if(Vector3.Distance(myTransform.position, shootPosition) > MIN_STRAIGHT)
                straight = false;
        }
        else {
            movementDir = MyRotateTowards(movementDir, endArcDirection/*shootPosition - myTransform.position*/, 
                angleSpeed * Time.deltaTime);   
        }
        movementDir.Normalize();
    }

    public static Vector3 MyRotateTowards(Vector3 current, Vector3 target, float maxAngle, bool clockwise = false) {
        float angle = Vector3.Angle(current, target);
        //Debug.Log("angle = " + angle);
        if(angle < maxAngle) {
            //Debug.Log("returned");
            return target;
        }
        int sign = clockwise ? -1 : 1;
        return Quaternion.AngleAxis(maxAngle, Vector3.forward * sign) * current;
    }

    internal override bool HitEnemy(EnemyBehav enemy, bool deleteWhenHit) {
        return base.HitEnemy(enemy, false);
    }

    internal override void TouchOutside(Vector3 wallPosition) {
    }


    void OnDrawGizmos() {
        if(currentState == (int)FruitState.shot) {
            Gizmos.color = Color.yellow;
            //Gizmos.DrawLine(myTransform.position, myTransform.position + movementDir * 4);

            Gizmos.color = Color.green;
            Gizmos.DrawLine(shootPosition, shootPosition + (startArcDirection * MIN_STRAIGHT));

            Gizmos.color = Color.red;
            Gizmos.DrawLine(shootPosition - (endArcDirection * MIN_STRAIGHT), shootPosition);

            Gizmos.DrawSphere(pivot, 0.3f);
        }
    }
}
