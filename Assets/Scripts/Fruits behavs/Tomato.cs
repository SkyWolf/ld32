﻿using UnityEngine;
using System.Collections;

public class Tomato : FruitFire {

    internal override bool HitEnemy(EnemyBehav enemy, bool deleteWhenHit = true) {
        bool success = enemy.HitByFruit(DAMAGE, true);
        if(success)
            DeleteByScene();
        return success;
    }

}
