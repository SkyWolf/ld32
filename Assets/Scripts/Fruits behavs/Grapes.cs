﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grapes : FruitFire {

    public Transform piecesContainer;
    public int PIECES_NUMBER;

    public float TOTAL_FIRE_ANGULATION;
    public float TIME_TO_EXPLODE;
    private float explodeTimeCollector;

    void Awake() {
        base.Awake();

        RefillPieces();
    }

    void OnEnable() {
        RefillPieces();
    }

    internal void RefillPieces() {
        int deltaPieces = PIECES_NUMBER - piecesContainer.childCount;
        for(int i = 0; i < deltaPieces; i++) {
            GameObject piece = fruitPools.GetFruit(FruitPoolMan.FruitType.grapePiece);
            piece.SetActive(false);
            piece.transform.parent = piecesContainer;
        }
    }

    internal override bool HitEnemy(EnemyBehav enemy, bool deleteWhenHit = true) {
        bool success = base.HitEnemy(enemy, false);
        if(success) {
            SplitElements();
        }

        return success;
    }

    internal override void EnterShot() {
        base.EnterShot();

        explodeTimeCollector = 0f;
    }

    internal override void UpdateShot(){
        explodeTimeCollector += Time.deltaTime;
        if(explodeTimeCollector < TIME_TO_EXPLODE) {
            base.UpdateShot();
        }
        else {
            explodeTimeCollector = 0f;
            SplitElements();
        }
    }

    private void SplitElements() {
        int numChildren = piecesContainer.childCount;
        Vector3 direction = Quaternion.AngleAxis(TOTAL_FIRE_ANGULATION / 2f, myTransform.forward) * shootDir;
        direction.Normalize();
        float deltaAngle = TOTAL_FIRE_ANGULATION / (numChildren - 1);
        for(int i = numChildren - 1; i >= 0; i--) {
            GrapePiece piece = piecesContainer.GetChild(i).GetComponent<GrapePiece>();
            Vector3 elemDirection = Quaternion.AngleAxis(i * deltaAngle, -myTransform.forward) * direction;
            piece.PlayShootInDirection(elemDirection, myTransform.position);
        }

        ChangeState((int)FruitState.idle);
        DeleteByScene();
    }

    public Transform GetGrapesPiecesContainerT() {
        return piecesContainer;
    }

}
