﻿using UnityEngine;
using System.Collections;

public class MelonPiece : FruitFire {

    public float MAXIMUM_EXPLOSION_ANGLE;
    public float MAX_RAND_EXPLOSION_SPEED;

    private GameObject lastEnemyToIgnore;

    public void EnableAndShootAfterExplosion(Vector3 explosionDir, GameObject enemyToIgnore) {

        myTransform.localPosition = Vector3.zero;

        //calculate a random angle with a maximum delta
        float randomAngle = Random.value * MAXIMUM_EXPLOSION_ANGLE * 2;
        randomAngle -= MAXIMUM_EXPLOSION_ANGLE;

        shootDir = movementDir = Quaternion.AngleAxis(randomAngle, -Vector3.forward) * explosionDir;
        shootDir.Normalize();

        //calculate a random starting position angle
        float randomRotation = Random.Range(0, 360);
        myTransform.localRotation = Quaternion.AngleAxis(randomRotation, Vector3.forward) * myTransform.localRotation;

        movementSpeed = SHOOT_SPEED + Random.value * MAX_RAND_EXPLOSION_SPEED;
        this.gameObject.SetActive(true);

        //detach from melon
        myTransform.parent = null;

        this.lastEnemyToIgnore = enemyToIgnore;

        ChangeState((int)FruitState.shot);
    }

    internal override bool HitEnemy(EnemyBehav enemy, bool deleteWhenHit = true) {
        if(enemy == lastEnemyToIgnore)
            return false;

        return base.HitEnemy(enemy, deleteWhenHit);
    }

    internal override void EnterEntering() {
        base.EnterEntering();

        lastEnemyToIgnore = null;
    }
}
