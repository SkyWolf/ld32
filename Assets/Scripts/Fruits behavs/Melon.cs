﻿using UnityEngine;
using System.Collections;

public class Melon : FruitFire {

    public Transform piecesContainer;
    public int PIECES_AFTER_EXPLOSION;

    void Awake(){
        base.Awake();

        RefillPieces();
    }

    void OnEnable() {
        RefillPieces();
    }

    internal void RefillPieces() {
        int deltaPieces = PIECES_AFTER_EXPLOSION - piecesContainer.childCount;
        for(int i = 0; i < deltaPieces; i++) {
            GameObject piece = fruitPools.GetFruit(FruitPoolMan.FruitType.melonPiece);
            piece.SetActive(false);
            piece.transform.parent = piecesContainer;
        }
    }

    internal override bool HitEnemy(EnemyBehav enemy, bool deleteWhenHit) {
        if(!base.HitEnemy(enemy, false))
            return false;
        Explode(enemy.GetPosition(), enemy.gameObject);

        return true;
    }

    internal override void TouchOutside(Vector3 wallPosition) {
        Explode(myTransform.position + wallPosition, null);
    }

    //bomb effect
    public void Explode(Vector3 collisionPosition, GameObject enemy) {
        Vector3 explosionDirection = myTransform.position - collisionPosition;

        int piecesCount = piecesContainer.childCount;
        for(int i = piecesCount - 1; i >= 0; i--) {
            MelonPiece pieceS = piecesContainer.GetChild(i).GetComponent<MelonPiece>();
            pieceS.EnableAndShootAfterExplosion(explosionDirection, enemy);
        }

        DeleteByScene();
    }
}
