﻿using UnityEngine;
using System.Collections;

public class FruitFire : BaseSM {
    public FruitPoolMan.FruitType myType;

    //game object's attributes
    internal Transform myTransform;
    internal Collider2D myCollider;

    internal static Orbiting mainOrbit;

    //entering params
    private float angleToFix;

    //movement params
    internal Vector3 movementDir;
    internal float movementSpeed;

    //looping values
    public float FIXING_ANGLE_SPEED;
    public float LOOP_SPEED;

    //shooting values
    internal Vector3 shootDir;
    public float SHOOT_SPEED;
    public float ROTATION_SPEED_IN_SHOOT = 90;

    //strike params 
    internal Transform hitT;
    internal Vector3 shootPosition;

    //spawn parameters
    public float TIME_TO_SPAWN = 1f;
    private float timeCollector = 0;
    private Vector3 scaleFactor = Vector3.zero;

    //dead parameters
    public float TIME_TO_DIE = 0.3f;
    private float scalingInDeadStart;

    //idle params
    public float TIME_TO_LIVE = 3f;
    private int rotateIdleSense;
    private float idleRotationSpeed;

    //damage attrs
    public int DAMAGE = 1;

    internal static FruitPoolMan fruitPools;

    internal enum FruitState {
        spawn,
        idle, 
        entering,
        looping,
        shot,
        dead,
        count
    }

    internal void Awake() {
        //caching 
        myTransform = this.transform;
        myCollider = this.GetComponent<Collider2D>();

        if(mainOrbit == null)
            mainOrbit = GameObject.FindGameObjectWithTag("MainChar").GetComponentInChildren<Orbiting>();

        if(fruitPools == null)
            fruitPools = GameObject.FindGameObjectWithTag("Pools").GetComponent<ObjectPoolList>().fruitPools;

        Initialize((int)FruitState.count, (int)FruitState.spawn);
    }

    internal override void Config() {
        enterArray[(int)FruitState.spawn] = EnterSpawn;
        updateArray[(int)FruitState.spawn] = UpdateSpawn;

        enterArray[(int)FruitState.idle] = EnterIdle;
        updateArray[(int)FruitState.idle] = UpdateIdle;
        exitArray[(int)FruitState.idle] = ExitIdle;
        
        updateArray[(int)FruitState.looping] = UpdateLooping;

        enterArray[(int)FruitState.entering] = EnterEntering;
        updateArray[(int)FruitState.entering] = UpdateEntering;

        enterArray[(int)FruitState.shot] = EnterShot;
        updateArray[(int)FruitState.shot] = UpdateShot;
        exitArray[(int)FruitState.shot] = ExitShot;

        enterArray[(int)FruitState.dead] = EnterDead;
        updateArray[(int)FruitState.dead] = UpdateDead;
    }


    public Vector3 GetPosition() {
        return myTransform.position;
    }

    //spawn phase

    public void Spawn(Vector3 spawnPosition) {
        myTransform.position = spawnPosition;
        myTransform.localScale = Vector3.zero;
        ChangeState((int)FruitState.spawn);
    }

    internal void EnterSpawn() {
        myCollider.enabled = false;

        timeCollector = 0;
        scaleFactor = Vector3.zero;
        myTransform.localScale = scaleFactor;
    }
    internal void UpdateSpawn() {
        timeCollector += Time.deltaTime;

        float scaleMultiplier = Mathf.SmoothStep(0, 1.5f, timeCollector / TIME_TO_SPAWN);
        //Dirty hack to be quick
        if(scaleMultiplier > 1.25f)
            scaleMultiplier = scaleMultiplier - ((scaleMultiplier % 1.25f) * 2);

        scaleFactor = Vector3.one * scaleMultiplier;
        myTransform.localScale = scaleFactor;

        if(timeCollector >= TIME_TO_SPAWN) {
            ChangeState((int)FruitState.idle);
        }
    }

    //dead phase
    internal void EnterDead() {
        myCollider.enabled = false;

        timeCollector = 0;
        scaleFactor = myTransform.localScale;
        scalingInDeadStart = myTransform.localScale.x;
    }
    internal void UpdateDead() {
        timeCollector += Time.deltaTime;

        float scaleMultiplier = Mathf.SmoothStep(scalingInDeadStart, 0f, timeCollector / TIME_TO_DIE);

        scaleFactor = Vector3.one * scaleMultiplier;
        myTransform.localScale = scaleFactor;

        if(timeCollector >= TIME_TO_DIE) {
            ChangeState((int)FruitState.idle);
            DeleteByScene();
        }
    }

    //idle phase
    internal void EnterIdle() {
        myCollider.enabled = true;
        timeCollector = 0f;
        idleRotationSpeed = Random.RandomRange(0, 20f);
        idleRotationSpeed += 10f;
        rotateIdleSense = Random.value >= 0.5f ? 1 : -1; 
    }
    internal void UpdateIdle() {

        //rotate
        myTransform.localRotation = Quaternion.AngleAxis(idleRotationSpeed * Time.deltaTime,
            myTransform.forward * rotateIdleSense) * myTransform.localRotation;

        timeCollector += Time.deltaTime;
        if(timeCollector >= TIME_TO_LIVE) {
            ChangeState((int)FruitState.dead);
        }
    }
    internal void ExitIdle() {
        myCollider.enabled = false;
    }

    // entering in orbit phase
    public void EnterInOrbit(float angle = 0f) {
        this.angleToFix = angle;
        if((int)FruitState.looping == currentState)
            return;

        ChangeState((int)FruitState.entering);
    }

    internal virtual void EnterEntering(){
    }

    private void UpdateEntering() {
        Vector3 inOrbitPosition = mainOrbit.GetInOrbitProjectedPosition(myTransform.position);
        if(inOrbitPosition == myTransform.position) {
            ChangeState((int)FruitState.looping);
        }
        else {
            //move towards the correct position
            myTransform.position = Vector3.MoveTowards(myTransform.position, inOrbitPosition,
                SHOOT_SPEED * Time.deltaTime);
        }
    }

    // looping phase
    internal void UpdateLooping() {
        myTransform.rotation = Quaternion.AngleAxis(LOOP_SPEED * Time.deltaTime, -myTransform.forward) 
            * myTransform.rotation;

        //check if there's an angle to fix respect to the orbit
        if(angleToFix != 0) {
            float absAngleMovement = Mathf.Min(Mathf.Abs(angleToFix), FIXING_ANGLE_SPEED * Time.deltaTime);
            float angleMovement = absAngleMovement * Mathf.Sign(angleToFix);
            angleToFix -= angleMovement;
            myTransform.localPosition = Quaternion.AngleAxis(angleMovement, -myTransform.forward) * myTransform.localPosition;
        }
    }

    //shooting
    public void PlayShoot(Vector3 shootStartingPosition) {
        if(currentState != (int)FruitState.looping)
            return;

        shootDir = myTransform.position - shootStartingPosition;
        shootDir.Normalize();

        myTransform.parent = null;

        shootPosition = shootStartingPosition;

        ChangeState((int)FruitState.shot);
    }

    internal virtual void EnterShot() {
        movementSpeed = SHOOT_SPEED;
        movementDir = shootDir;
        StartCoroutine(EnableCollider());
    }
    internal virtual void UpdateShot() {
        //translate
        myTransform.localPosition += movementDir * movementSpeed * Time.deltaTime;

        //and rotate
        myTransform.localRotation = Quaternion.AngleAxis(ROTATION_SPEED_IN_SHOOT * Time.deltaTime, -Vector3.forward) *
            myTransform.localRotation;
    }
    internal virtual void ExitShot() {
        myCollider.enabled = false;
    }
    IEnumerator EnableCollider() {
        yield return new WaitForSeconds(0.2f);
        myCollider.enabled = true;
    }

    //other stuff

    void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Enemy") {
            if(currentState != (int)FruitState.shot)
                return;
            hitT = other.transform;
            HitEnemy(other.GetComponent<EnemyBehav>());
        }
        else if(other.gameObject.layer == LayerMask.NameToLayer("Orbit")) {
            //Debug.Log("collision with character");
            EnterIntoChar();
        }
        else if(other.tag == "OutsideTrigger") {
            BoxCollider2D boxCollider = (BoxCollider2D)other;
            if(boxCollider.offset.x != 0f) {
                if(boxCollider.offset.x > 0f)
                    TouchOutside(Vector3.right);
                else
                    TouchOutside(Vector3.left);
            }
            else {
                if(boxCollider.offset.y > 0f)
                    TouchOutside(Vector3.up);
                else
                    TouchOutside(Vector3.down);
            }
        }
    }

    internal virtual void EnterIntoChar() {
        mainOrbit.AddExistingFruit(myTransform);
    }

    internal virtual void TouchOutside(Vector3 outsidePosition) {
        DeleteByScene();
    }

    /// <summary>
    /// return true if the attack has success
    /// </summary>
    internal virtual bool HitEnemy(EnemyBehav enemy, bool deleteWhenHit = true){
        bool success = enemy.HitByFruit(DAMAGE);
       // Debug.Log("enemy hit with success = " + success);
        if(success && deleteWhenHit)
            DeleteByScene();
        return success;
    }

    public void DeleteByScene() {
        //Debug.Log("deleted by scene");
        currentState = (int)FruitState.idle;
        fruitPools.ReturnFruit(this.gameObject, myType);
    }
}
