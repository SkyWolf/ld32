﻿using UnityEngine;
using System.Collections;

public class Coconut : FruitFire {

    internal override bool HitEnemy(EnemyBehav enemy, bool deleteWhenHit) {
        return base.HitEnemy(enemy, false);
    }

}
