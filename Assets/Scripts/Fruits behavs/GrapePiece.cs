﻿using UnityEngine;
using System.Collections;

public class GrapePiece : FruitFire {

    public void PlayShootInDirection(Vector3 direction, Vector3 bornPosition) {
        this.gameObject.SetActive(true);
        movementDir = shootDir = direction.normalized;
        myTransform.parent = null;
        myTransform.position = bornPosition;
        ChangeState((int)FruitState.shot);
    }

    internal override bool HitEnemy(EnemyBehav enemy, bool deleteWhenHit = true) {
        if(base.HitEnemy(enemy, true)) {
            //assign to grapes parent
            return true;
        }
        return false;
    }

    internal override void EnterIntoChar() {
    }

}
