﻿using UnityEngine;
using System.Collections;

public class SpriteParticle : MonoBehaviour {
    private Transform myTransform;

    public float minParticleFrequency;
    public float maxParticleFrequency;
    public float particleLifetime;
    public float width;
    public float speed;
    public Sprite particleType;

    public Color particleColor;

    private float timeCollector;
    private float nextParticleIn = -1;

    private ObjectPool myPool;

    void Awake() {
        myTransform = transform;
        myPool = GameObject.FindGameObjectWithTag("Pools").GetComponent<ObjectPoolList>().particlePool;

        timeCollector = 0;
    }

    void Update() {
        timeCollector += Time.deltaTime;

        if(timeCollector >= nextParticleIn) {
            ParticleLife pl = myPool.GetOne().GetComponent<ParticleLife>();
            float randomX = (Random.value * width) - (width / 2);
            pl.Live(    particleType,
                        particleColor,
                        myTransform.position + new Vector3(randomX, 0, 0),
                        -Vector3.up,
                        speed,
                        particleLifetime,
                        myPool  );

            nextParticleIn = (Random.value * (maxParticleFrequency - minParticleFrequency)) + minParticleFrequency;

            timeCollector = 0;
        }
    }
}
