﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterBehav : MonoBehaviour {

    private Transform myTransform;
    private Rigidbody2D myRigidbody;
    private AudioSource mySource;

    //character's attributes
    public float ACCELERATION, DECELERATION;
    public float MAX_SPEED;
    public float MAX_ANGLE_PER_SEC;

    public int MAX_HEALTH;
    public int currentHealth;// { get; private set; }

    public AudioClip playerHit;

    private Vector3 currentSpeed;
    private Vector3 lastMovDir;

    private Dictionary<string, Vector3> controlDir = 
        new Dictionary<string, Vector3>{
            {"Up" , Vector3.up},
            {"Left" , Vector3.left},
            {"Down" , Vector3.down},
            {"Right" , Vector3.right}
    };

    [HideInInspector]
    public bool disableMovement = false;

    public bool registerHowMuchMovement = false;
    [HideInInspector]
    public float howMuchIMoved;

	// Use this for initialization
	void Awake () {
        myTransform = this.transform;
        myRigidbody = this.GetComponent<Rigidbody2D>();
        mySource = GetComponent<AudioSource>();

        currentSpeed = Vector3.zero;
        currentHealth = MAX_HEALTH;
	}

    public void HitByEnemy(int damage) {
        //TODO

        mySource.PlayOneShot(playerHit);
        currentHealth = Mathf.Max(0, currentHealth - damage);
        /*if(currentHealth == 0)
            Debug.Log("game over");*/
    }
	
	// Update is called once per frame
	void Update () {
        if(!disableMovement) {
            currentSpeed = myRigidbody.velocity;

            Vector3 inputDirections = Vector3.zero;
            foreach(string key in controlDir.Keys) {
                if(Input.GetButton(key)) {
                    inputDirections += controlDir[key];
                    currentSpeed += controlDir[key] * ACCELERATION * Time.deltaTime;
                    if(Mathf.Abs(currentSpeed.x) > MAX_SPEED)
                        currentSpeed.x = MAX_SPEED * Mathf.Sign(currentSpeed.x);
                    if(Mathf.Abs(currentSpeed.y) > MAX_SPEED)
                        currentSpeed.y = MAX_SPEED * Mathf.Sign(currentSpeed.y);
                }
            }

            if(inputDirections.x == 0) {
                //decelerate in x direction
                if(currentSpeed.x > 0)
                    currentSpeed.x = Mathf.Max(0f, currentSpeed.x - DECELERATION * Time.deltaTime);
                else
                    currentSpeed.x = Mathf.Min(0f, currentSpeed.x + DECELERATION * Time.deltaTime);
            }
            if(inputDirections.y == 0) {
                //decelerate in y direction
                if(currentSpeed.y > 0)
                    currentSpeed.y = Mathf.Max(0f, currentSpeed.y - DECELERATION * Time.deltaTime);
                else
                    currentSpeed.y = Mathf.Min(0f, currentSpeed.y + DECELERATION * Time.deltaTime);
                
            }

            myRigidbody.velocity = currentSpeed;
            if(currentSpeed != Vector3.zero) {
                Vector3 timedSpeed = currentSpeed * Time.deltaTime;
                if(registerHowMuchMovement)
                    howMuchIMoved += timedSpeed.magnitude;
            }
        }
	}
}
