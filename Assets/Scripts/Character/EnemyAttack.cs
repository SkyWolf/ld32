﻿using UnityEngine;
using System.Collections;

public abstract class EnemyAttack : MonoBehaviour {

    internal Transform myTransform;
    internal static Transform characterT;
    internal AudioSource mySource;

    public AudioClip soundWhenAttacking;
    public float SPEED = 4f;

    internal void Awake() {
        if(characterT == null)
            characterT = GameObject.FindGameObjectWithTag("MainChar").transform;

        myTransform = transform;
        mySource = GetComponent<AudioSource>();
    }

    internal void Update() {
        Vector3 diff = characterT.position - myTransform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        myTransform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
    }

    public void StartAttack() {
        this.enabled = true;
    }

    public void StopAttack() {
        this.enabled = false;
    }

}
