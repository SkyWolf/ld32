﻿using UnityEngine;
using System.Collections;

public class BeeAttack : EnemyAttack {
    //stuff to set by inspector for the movement
    public float EDGE_DISTANCE = 0.7f;

    private float timeCollector;
    private float timeToReach;

    private Vector3 targetStep;
    private Vector3 startPosition;
    private Vector3 currentDirection;

    void OnEnable() {
        currentDirection = Vector3.zero;
        CalculateNextTmpTarget();
    }

    // Update is called once per frame
    void Update() {
        base.Update();

        timeCollector += Time.deltaTime;
        myTransform.position = Vector3.Lerp(startPosition, targetStep, timeCollector / timeToReach);

        if(myTransform.position == targetStep) {
            CalculateNextTmpTarget();
        }
    }

    private void CalculateNextTmpTarget() {
        //calculate data
        timeCollector = 0f;
        timeToReach = EDGE_DISTANCE / (SPEED);

        Vector3 toCharacterDir = characterT.position - myTransform.position;

        Vector3 xVersor = Vector3.Project(toCharacterDir, Vector3.right);
        Vector3 yVersor = Vector3.Project(toCharacterDir, Vector3.up);

        if(xVersor.sqrMagnitude > yVersor.sqrMagnitude) {
            if(currentDirection != xVersor.normalized)
                currentDirection = xVersor.normalized;
            else
                currentDirection = yVersor.normalized;
        }
        else {
            if(currentDirection != yVersor.normalized)
                currentDirection = yVersor.normalized;
            else
                currentDirection = xVersor.normalized;
        }

        targetStep = myTransform.position + (currentDirection * EDGE_DISTANCE);
        startPosition = myTransform.position;
    }

}
