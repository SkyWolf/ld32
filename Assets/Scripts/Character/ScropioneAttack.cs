﻿using UnityEngine;
using System.Collections;

public class ScropioneAttack : EnemyAttack {
    //stuff to set by inspector for the movement
    public float VERTICAL_EDGE_DISTANCE = 2f;
    public float HORIZ_DISTANCE_BY_CENTER = 3f;
    private bool isHorizMovement;

    private float timeCollector;
    private float timeToReach;

    private Vector3 targetStep;
    private Vector3 startPosition;

    public GameObject bulletPrefab;
    public Transform leftShotPos, rightPosShot;
    private bool leftFire = false;

    public float FIRE_TIME = 1f;
    private float fireTimeCollector;

    void OnEnable() {
        isHorizMovement = true;
        fireTimeCollector = 0f;
        CalculateNextTmpTarget();
    }

    // Update is called once per frame
    void Update() {
        base.Update();

        timeCollector += Time.deltaTime;
        myTransform.position = Vector3.Lerp(startPosition, targetStep, timeCollector / timeToReach);

        if(myTransform.position == targetStep) {
            CalculateNextTmpTarget();
        }

        fireTimeCollector += Time.deltaTime;
        if(fireTimeCollector > FIRE_TIME) {
            fireTimeCollector = 0f;
            Fire();
        }
    }

    private void CalculateNextTmpTarget() {
        //calculate data
        timeCollector = 0f;

        isHorizMovement = !isHorizMovement;

        if(isHorizMovement) {
            Vector3 myCenteredPosition = myTransform.position;
            myCenteredPosition.x = 0;
            Vector3 toMyCenterDir = (myCenteredPosition - myTransform.position).normalized;
            targetStep = myCenteredPosition + (toMyCenterDir * HORIZ_DISTANCE_BY_CENTER);
            timeToReach = (HORIZ_DISTANCE_BY_CENTER * 2f) / (SPEED);
        }
        else {
            //move in vertical in direction of the target
            Vector3 myPositionInLine = myTransform.position;
            myPositionInLine.x = characterT.position.x;
            Vector3 toCharacterVerticalDir = (characterT.position - myPositionInLine).normalized;
            targetStep = myTransform.position + (toCharacterVerticalDir * VERTICAL_EDGE_DISTANCE);
            timeToReach = VERTICAL_EDGE_DISTANCE / (SPEED);
        }

        startPosition = myTransform.position;
    }

    public void Fire() {
        mySource.PlayOneShot(soundWhenAttacking);

        Vector3 firePos = leftFire ? leftShotPos.position : rightPosShot.position;
        GameObject bullet = (GameObject)GameObject.Instantiate(bulletPrefab);
        bullet.GetComponent<BulletLife>().Fire(firePos, characterT.position - firePos);
        leftFire = !leftFire;
    }
}
