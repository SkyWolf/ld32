﻿using UnityEngine;
using System.Collections;

public class CharacterHitContact : MonoBehaviour {

    private float timeCollector;
    private bool isHit = false;
    public float TIME_INVULNERABLE = 1.0f;

    public CharacterBehav charBehav;

    void OnTriggerEnter2D(Collider2D collider) {
        if(isHit)
            return;
        if(collider.gameObject.layer == LayerMask.NameToLayer("Character")) {
            isHit = true;
            charBehav.HitByEnemy(1);
        }
    }

    void Update() {
        if(isHit) {
            timeCollector += Time.deltaTime;
            if(timeCollector > TIME_INVULNERABLE) {
                isHit = false;
                timeCollector = 0f;
            }
        }
    }
}
