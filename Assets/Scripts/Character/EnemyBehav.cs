﻿using UnityEngine;
using System.Collections;

public class EnemyBehav : BaseSM {
    //GO attrs
    internal Transform myTransform;
    internal EnemyAttack myAttack;
    internal SpriteRenderer myRenderer;
    internal BoundsDescriptor theBounds;
    internal ObjectPool lifeFlasherPool;
    internal AudioSource myAudioSource;
    internal Transform theCharacter;

    //enemy's attrs
    public int STARTING_LIFE;
    public int currentLife;

    //substates
    internal bool isHit;
    internal float hitTimeCollector;
    public float TIME_AFTER_HIT = 1.0f;

    //die state
    public float TIME_TO_DIE = 1.5f;
    public float SPIRAL_SPEED = 200.0f;
    private float timeCollectorDie;
    private float angleCollectorDie;

    //hit state
    public float TIME_TO_BLINK = 1;
    public int BLINKS = 3;
    public Color blinkColor;
    private Color normalColor;
    private float blinkerTime;
    private float blinkCollector = 0;
    public bool HAS_TO_STOP_WHEN_HIT = false;

    //get in position
    public float HOW_MUCH_INTO_BOUNDS = 0.4f;
    private float collectorHowMuch = 0;
    private bool intoBounds = false;

    public bool hasLookAt = true;

    public AudioClip soundWhenHit;
    public AudioClip soundWhenDieing;

    public enum EnemyState {
        spawn, 
        getinposition,
        live,
        hit,
        dead,
        size
    }

    void Awake() {
        myTransform = this.transform;
        myAttack = this.GetComponent<EnemyAttack>();
        myRenderer = this.GetComponent<SpriteRenderer>();
        theBounds = GameObject.FindGameObjectWithTag("OutsideTrigger").GetComponent<BoundsDescriptor>();
        lifeFlasherPool = GameObject.FindGameObjectWithTag("Pools").GetComponent<ObjectPoolList>().lifeFlasherPool;
        myAudioSource = this.GetComponent<AudioSource>();
        theCharacter = GameObject.FindGameObjectWithTag("MainChar").transform;
        Initialize((int)EnemyState.size, (int)EnemyState.spawn);
    }

    void Start() {
        Spawn(myTransform.position);
    }

    internal override void Config() {

        enterArray[(int)EnemyState.spawn] = EnterSpawn;
        updateArray[(int)EnemyState.spawn] = UpdateSpawn;

        enterArray[(int)EnemyState.getinposition] = EnterGetInPosition;
        updateArray[(int)EnemyState.getinposition] = UpdateGetInPosition;

        enterArray[(int)EnemyState.live] = EnterLive;
        updateArray[(int)EnemyState.live] = UpdateLive;
        exitArray[(int)EnemyState.live] = ExitLive;

        enterArray[(int)EnemyState.hit] = EnterHit;
        updateArray[(int)EnemyState.hit] = UpdateHit;
        exitArray[(int)EnemyState.hit] = ExitHit;

        enterArray[(int)EnemyState.dead] = EnterDead;
        updateArray[(int)EnemyState.dead] = UpdateDead;
    }

    public Vector3 GetPosition() {
        return myTransform.position;
    }

    //spawn phase
    public void Spawn(Vector3 spawnPosition) {
        if(myTransform == null)
            myTransform = transform;
        myTransform.position = spawnPosition;
        ChangeState((int)EnemyState.spawn);
        currentLife = STARTING_LIFE;
    }
    private void EnterSpawn() {
    }
    private void UpdateSpawn() {
        ChangeState((int)EnemyState.getinposition);
    }

    private void EnterGetInPosition() {
        intoBounds = false;
        collectorHowMuch = 0;
    }

    //get in position
    private void UpdateGetInPosition() {
        float movement = myAttack.SPEED * Time.deltaTime;
        if(theBounds.AmIInBounds(myTransform.position))
            collectorHowMuch += movement;

        if(myTransform.position.x < 0)
            myTransform.position += Vector3.right * movement;
        else
            myTransform.position += -Vector3.right * movement;

        if(hasLookAt)
            LookAtPosition();

        if(collectorHowMuch > HOW_MUCH_INTO_BOUNDS)
            ChangeState((int)EnemyState.live);
    }

    private void LookAtPosition() {
        Vector3 diff = theCharacter.position - myTransform.position;
        diff.Normalize();
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        myTransform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
    }

    //idle phase
    private void UpdateIdle() {
    }

    //move phase
    public void StartLive() {
        if(currentState == (int)EnemyState.live)
            return;
    }
    internal virtual void EnterLive() {
        myAttack.StartAttack();
    }
    internal virtual void UpdateLive() {

    }

    internal virtual void EnterHit() {
        normalColor = myRenderer.color;
        blinkerTime = TIME_TO_BLINK / BLINKS;
        blinkCollector = 0;

        if(HAS_TO_STOP_WHEN_HIT)
            myAttack.StopAttack();
    }
    internal virtual void UpdateHit() {
        blinkCollector += Time.deltaTime;
        myRenderer.color = Color.Lerp(normalColor, blinkColor, Mathf.PingPong(blinkCollector % blinkerTime, 1));
        if(blinkCollector > TIME_TO_BLINK)
            ChangeState((int)EnemyState.live);
    }
    internal virtual void ExitHit() {
        myRenderer.color = normalColor;

        if(HAS_TO_STOP_WHEN_HIT)
            myAttack.StartAttack();
    }

    internal virtual void ExitLive() {
    }

    //dead phase
    internal void EnterDead() {
        myAttack.StopAttack();
        timeCollectorDie = 0;
        angleCollectorDie = 0;
    }
    internal void UpdateDead() {
        timeCollectorDie += Time.deltaTime;
        angleCollectorDie += SPIRAL_SPEED * Time.deltaTime;

        myTransform.rotation = Quaternion.AngleAxis(angleCollectorDie, Vector3.forward);
        float apply = Mathf.SmoothStep(1, 0, timeCollectorDie / TIME_TO_DIE);
        myTransform.localScale = new Vector3(apply, apply, 1);

        if(timeCollectorDie >= TIME_TO_DIE)
            Destroy(gameObject);
    }

    //hit phase
    public bool HitByFruit(int damage, bool glued = false) {
        if(currentState == (int)EnemyState.dead)
            return false;

        currentLife -= damage;
        if(currentLife > 0) {
            ChangeState((int)EnemyState.hit);
        }
        else{
            ChangeState((int)EnemyState.dead);
            myAudioSource.PlayOneShot(soundWhenDieing);
            ScoreManager.AddPoint();
        }
        myAudioSource.PlayOneShot(soundWhenHit);

        GameObject lf = lifeFlasherPool.GetOne();
        lf.GetComponent<FollowTarget>().Follow(myTransform);
        lf.GetComponent<HeartVisualize>().Visualize(currentLife, STARTING_LIFE);

        return true;
    }
}
