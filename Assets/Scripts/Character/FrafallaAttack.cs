﻿using UnityEngine;
using System.Collections;

public class FrafallaAttack : EnemyAttack {
    //stuff to set by inspector for the movement
    public float EDGE_DISTANCE = 1.5f;
    public float ATTACK_SPEED_MULTIPLIER = 1.5f;
    private float MIN_DISTANCE_TO_ATTACK = 1.0f;
    public float ZIGZAG_ANGLE;

    private float timeCollector;
    private float timeToReach;

    private Vector3 targetStep;
    private Vector3 startPosition;
    private int left = -1;

    public GameObject bulletPrefab;
    public Transform leftShotPos, rightPosShot;
    private bool leftFire = false;

    public float FIRE_TIME = 1f;
    private float fireTimeCollector;

    void OnEnable() {
        fireTimeCollector = 0f;
        CalculateNextTmpTarget();
    }
	
	// Update is called once per frame
	void Update () {
        base.Update();

        timeCollector += Time.deltaTime;
        myTransform.position = Vector3.Lerp(startPosition, targetStep, timeCollector / timeToReach);

        if(myTransform.position == targetStep) {
            CalculateNextTmpTarget();
        }

        fireTimeCollector += Time.deltaTime;
        if(fireTimeCollector > FIRE_TIME) {
            fireTimeCollector = 0f;
            Fire();
        }
	}

    private void CalculateNextTmpTarget() {
        //calculate data
        timeCollector = 0f;

        Vector3 toCharacterDir = characterT.position - myTransform.position;

        if(toCharacterDir.sqrMagnitude > Mathf.Pow(MIN_DISTANCE_TO_ATTACK, 2f)) {
            toCharacterDir = Quaternion.AngleAxis(ZIGZAG_ANGLE * left, Vector3.forward) *
            toCharacterDir;
            timeToReach = EDGE_DISTANCE / (SPEED);
        }
        else
            timeToReach = EDGE_DISTANCE / (SPEED * ATTACK_SPEED_MULTIPLIER);

        toCharacterDir.Normalize();
        left = -left;
        
        targetStep = myTransform.position + (toCharacterDir * EDGE_DISTANCE);
        startPosition = myTransform.position;
    }

    public void Fire() {
        mySource.PlayOneShot(soundWhenAttacking);

        Vector3 firePos = leftFire ? leftShotPos.position : rightPosShot.position;
        GameObject bullet = (GameObject)GameObject.Instantiate(bulletPrefab);
        bullet.GetComponent<BulletLife>().Fire(firePos, characterT.position - firePos);
        leftFire = !leftFire;
    }
}
