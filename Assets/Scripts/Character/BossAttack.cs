﻿using UnityEngine;
using System.Collections;

public class BossAttack : EnemyAttack {
    //stuff to set by inspector for the movement
    public float HORIZ_DISTANCE_BY_CENTER = 4f;
    public float MIN_DISTANCE = 1f;

    private float timeCollector;
    private float timeToReach;

    private Vector3 targetStep;
    private Vector3 startPosition;

    private float timeLastAttackCollector;
    public float MIN_ATTACK_TIMER = 1.5f;

    private float deltaToAttack = 0.2f;

    private bool isFiring;
    private float fireTimeCollector;
    public float TIME_TO_FIRE = 2f;
    public float FIRE_LENGTH = 5f;
    private Vector3 fireCurrentposition;
    private Vector3 fireTarget;

    private int dir;

    private LineRenderer myLineRenderer;

    void OnEnable() {
        dir = -1;
        isFiring = false;
        CalculateNextTmpTarget();

        timeLastAttackCollector = 0f;
    }

    void Awake() {
        base.Awake();

        myLineRenderer = this.GetComponentInChildren<LineRenderer>();
    }

    // Update is called once per frame
    void Update() {
        if(!isFiring) {
            timeCollector += Time.deltaTime;
            myTransform.position = Vector3.Lerp(startPosition, targetStep, timeCollector / timeToReach);

            if(myTransform.position == targetStep) {
                CalculateNextTmpTarget();
            }

            timeLastAttackCollector += Time.deltaTime;
            if(timeLastAttackCollector > MIN_ATTACK_TIMER) {
                Vector3 myPositionInCharacter = myTransform.position;
                myPositionInCharacter.y = characterT.position.y;
                if(Mathf.Abs((myPositionInCharacter - characterT.position).magnitude) < deltaToAttack){
                    timeLastAttackCollector = 0f;
                    Fire();
                }
            }
        }
        else {
            fireTimeCollector += Time.deltaTime;
            fireCurrentposition = Vector3.Lerp(Vector3.zero, fireTarget, fireTimeCollector / TIME_TO_FIRE);
            myLineRenderer.SetPosition(1, fireCurrentposition);

            Vector3 tileToCharDir = characterT.position - myLineRenderer.transform.position;
            if(Mathf.Abs(fireCurrentposition.y) > Mathf.Abs(tileToCharDir.y)) {
                if(Mathf.Abs(tileToCharDir.x) < deltaToAttack) {
                    characterT.GetComponent<CharacterBehav>().HitByEnemy(2);
                    isFiring = false;
                    myLineRenderer.SetVertexCount(0);
                    return;
                }
            }

            if(fireCurrentposition == fireTarget) {
                isFiring = false;
                myLineRenderer.SetVertexCount(0);
            }
        }
    }

    private void CalculateNextTmpTarget() {
        //calculate data
        timeCollector = 0f;

        dir = -dir;
        Vector3 movementDir = Vector3.right * dir;
        float maxDistance = 0f;

        if(dir == 1) {
            maxDistance = HORIZ_DISTANCE_BY_CENTER - myTransform.position.x;
        }
        else {
            maxDistance = HORIZ_DISTANCE_BY_CENTER + myTransform.position.x;
        }

        float realMinDistance = Mathf.Min(MIN_DISTANCE, maxDistance);
        
        float randomDistance = Random.RandomRange(realMinDistance, maxDistance);
        targetStep = myTransform.position + (movementDir * randomDistance);
        timeToReach = randomDistance / (SPEED);

        startPosition = myTransform.position;
    }

    public void Fire() {
        fireTarget = Vector3.up * FIRE_LENGTH;
        fireCurrentposition = Vector3.zero;
        fireTimeCollector = 0f;
        isFiring = true;

        myLineRenderer.SetVertexCount(2);
        myLineRenderer.SetPosition(0, Vector3.zero);
        myLineRenderer.SetPosition(1, Vector3.zero);
    }
}
