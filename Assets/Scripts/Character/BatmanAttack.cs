﻿using UnityEngine;
using System.Collections;

public class BatmanAttack : EnemyAttack {

    public float RADIUS_AROUND_CHAR = 3f;
    public float ROTATION_SPEED = 180f;

    public GameObject bulletPrefab;
    public Transform shotPos;

    public float FIRE_TIME = 1f;
    private float fireTimeCollector;

    void OnEnable() {
        fireTimeCollector = 0f;
    }
	
	void Update () {
        base.Update();

        Vector3 fromCharToMeDir = myTransform.position - characterT.position;
        fromCharToMeDir.Normalize();
        fromCharToMeDir = Quaternion.AngleAxis(ROTATION_SPEED * Time.deltaTime, Vector3.forward) *
            fromCharToMeDir;

        Vector3 target = characterT.position + (fromCharToMeDir * RADIUS_AROUND_CHAR);
        myTransform.position = Vector3.MoveTowards(myTransform.position, target,
            SPEED * Time.deltaTime);

        fireTimeCollector += Time.deltaTime;
        if(fireTimeCollector > FIRE_TIME) {
            fireTimeCollector = 0f;
            Fire();
        }
	}

    public void Fire() {
        mySource.PlayOneShot(soundWhenAttacking);

        GameObject bullet = (GameObject)GameObject.Instantiate(bulletPrefab);
        bullet.GetComponent<BulletLife>().Fire(shotPos.position, characterT.position - shotPos.position);
    }
}
