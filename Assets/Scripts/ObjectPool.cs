﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour {
    private Transform myTransform;
    private Stack<GameObject> theStack = new Stack<GameObject>();

    public GameObject toPool;
    public int startWith;

    private bool isInitialized = false;

    void Awake() {
        myTransform = transform;

        Initialize();
    }

    public void Initialize() {
        if(startWith == 0 || isInitialized)
            return;

        isInitialized = true;
        GameObject t;
        for(int i = 0; i < startWith; i++) {
            t = (GameObject)Instantiate(toPool);
            t.transform.parent = myTransform;
            t.SetActive(false);
        }
    }

    public GameObject GetOne() {
        if(theStack.Count == 0)
            return (GameObject)Instantiate(toPool);

        GameObject popped = theStack.Pop();
        popped.transform.parent = null;
        popped.SetActive(true);
        
        return popped;
    }

    public void ReturnOne(GameObject toPush) {
        Transform toPushT = toPush.transform;
        toPushT.parent = myTransform;
        toPushT.localPosition = Vector3.zero;
        toPush.SetActive(false);
        theStack.Push(toPush);
    }
}