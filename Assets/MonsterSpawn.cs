﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(BoundsDescriptor))]
public class MonsterSpawn : MonoBehaviour {
    private BoundsDescriptor myBounds;
    private HorizontalSize cameraHS;

    public Transform monstersContainer;
    public GameObject[] monsters;
    public GameObject boss;

    public int maxMonstersOnField = 2;
    public float minTimeToSpawn = 1f;
    public float maxTimeToSpawn = 1.5f;

    public float howFarOffBorders = 0.5f;
    public float spawnableHeightPercentage = 0.8f;


    private float spawnNextIn;
    private float timeCollector;

    private bool bossTime = false;

    void Awake() {
        myBounds = GetComponent<BoundsDescriptor>();

        GameObject cameraGO = GameObject.FindGameObjectWithTag("MainCamera");
        cameraHS = cameraGO.GetComponent<HorizontalSize>();

        ResetTimer();
    }

    void ResetTimer() {
        timeCollector = 0;
        spawnNextIn = (Random.value * (maxTimeToSpawn - minTimeToSpawn)) + minTimeToSpawn;
    }

    private void SpawnBoss() {
        GameObject newMonster = (GameObject)Instantiate(boss);
        Transform newMonsterT = newMonster.transform;
        newMonsterT.parent = monstersContainer;

        float clampedHeight = Mathf.Clamp(myBounds.playFieldHeight * spawnableHeightPercentage, 0, myBounds.playFieldHeight);
        float x;
        x = -(cameraHS.horizontalOrthographicSize + howFarOffBorders);
        float y = 1.5f;

        newMonsterT.GetComponent<EnemyBehav>().Spawn(new Vector3(x, y, 0));

        bossTime = true;
    }

    void Update() {
        if(bossTime)
            return;
        if(maxMonstersOnField != 0 && monstersContainer.childCount >= maxMonstersOnField)
            return;

        if(ScoreManager.score >= ScoreManager.targetBeforeBoss) {
            SpawnBoss();
            return;
        }

        timeCollector += Time.deltaTime;

        if(timeCollector >= spawnNextIn) {
            int index = Random.Range(0, monsters.Length);

            GameObject newMonster = (GameObject)Instantiate(monsters[index]);
            Transform newMonsterT = newMonster.transform;
            newMonsterT.parent = monstersContainer;

            float clampedHeight = Mathf.Clamp(myBounds.playFieldHeight * spawnableHeightPercentage, 0, myBounds.playFieldHeight);
            float x;
            if(Random.value <= 0.5f)
                x = -(cameraHS.horizontalOrthographicSize + howFarOffBorders);
            else
                x = cameraHS.horizontalOrthographicSize + howFarOffBorders;
            float y = (Random.value * clampedHeight) - (clampedHeight / 2);

            newMonsterT.position = new Vector3(x, y, 0);

            ResetTimer();
        }
    }
}
