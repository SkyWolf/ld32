﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {
    [HideInInspector]
    public Transform target;
    public float howMuchUnder = 0.5f;

    public float timeToFlash;
    private float timeCollector;
    
    private Transform myTransform;
    private SpriteRenderer myRenderer;

    private ObjectPool lifeFlash;

    void Awake() {
        myTransform = transform;
        myRenderer = GetComponent<SpriteRenderer>();

        lifeFlash = GameObject.FindGameObjectWithTag("Pools").GetComponent<ObjectPoolList>().lifeFlasherPool;
    }

    public void Follow(Transform target) {
        SetAlpha(0);
        this.target = target;
        myTransform.position = target.position - (Vector3.up * howMuchUnder);
        timeCollector = 0;
    }

    private void SetAlpha(float a) {
        Color theColor = myRenderer.color;
        theColor.a = a;
        myRenderer.color = theColor;
    }
	
	// Update is called once per frame
	void Update () {
        if(target != null) {
            myTransform.position = target.position - (Vector3.up * howMuchUnder);

            timeCollector += Time.deltaTime;

            SetAlpha(Mathf.PingPong(timeCollector / (timeToFlash / 2), 1));

            if(timeCollector >= timeToFlash) {
                target = null;
                lifeFlash.ReturnOne(gameObject);
            }
        }
	}
}
