﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HeartVisualize : MonoBehaviour {
    public Sprite[] possibleConditions;
    public Sprite empty;

    public bool writeOnImage;
    public Image image;
    public SpriteRenderer spriteRenderer;

    public void Visualize(int currentHealth, int maxHealth) {
        Sprite toRender;
        if(currentHealth > 10)
            toRender = possibleConditions[9];
        else if(currentHealth <= 0)
            toRender = empty;
        else {
            float percentage = (float)currentHealth / Mathf.Min(10, maxHealth);
            toRender = possibleConditions[(int)(percentage * possibleConditions.Length) - 1];
        }

        if(writeOnImage)
            image.sprite = toRender;
        else
            spriteRenderer.sprite = toRender;
    }
}
